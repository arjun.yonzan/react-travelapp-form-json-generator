const ol = window.ol

/* -- ICON STYLE -- */
let iconStyle = new ol.style.Style({
  image: new ol.style.Icon({
    anchor: [.5,20],
    scale: 1.5,
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    src: 'marker.svg' 
  })

});

/*label style seperated....*/
let labelStyle = new ol.style.Style({
  text: new ol.style.Text({
    font: '14px Calibri,sans-serif',
    overflow: true,
    fill: new ol.style.Fill({
      color: '#000'
    }),
    stroke: new ol.style.Stroke({
      color: '#fff',
      width: 3
    }),
    offsetY: -40,
    // offsetX: 70
  })
});

/*style...*/
let style = [iconStyle, labelStyle];

export default function generateMap(mapDiv, place){
  if(place.locations){

    let locations = place.locations
    let locationsCopy = JSON.parse(JSON.stringify(locations));  

    if(!locations.length){ mapDiv.innerHTML=""; return;}

    let center = locations[locations.length-1].points.reverse()

    //clear old map hack and redo...
    mapDiv.innerHTML = ""
    let map = new ol.Map({
      target: mapDiv,
      interactions: ol.interaction.defaults({
        onFocusOnly: true
      }),

      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat(center),
        zoom: 10
      })
    }); // end map//


    let coordinates = [];

    // GENERATING MARKERS
    /*looping all the location.....points*/
    for(let i=0;i<locations.length; i++){

      let points = locations[i].points

      /*reverse back... weird js...*/
      if(i!==locations.length-1){
        points = points.reverse()
      }

      /*marker*/
      let layer = new ol.layer.Vector({
         source: new ol.source.Vector({
             features: [
                 new ol.Feature({
                     geometry: new ol.geom.Point(ol.proj.fromLonLat(points)),
                     name: `${i+1}. ${locations[i].name}`
                 })
             ]
         }),
         style: function(feature) {
          labelStyle.getText().setText(feature.get('name'));
          return style;
        }

     });

      /*create line path conditional*/
      if(i!==locations.length-1){
        let pointsA = locations[i].points
        let pointsB = locationsCopy[i+1].points.reverse()
        createLine(pointsA,pointsB,map)
      }
          
      map.addLayer(layer);

      coordinates.push(points)
    }//end for

    let boundingExtent = ol.extent.boundingExtent(coordinates); 
    boundingExtent = ol.proj.transformExtent(boundingExtent, ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857'));

    const view = map.getView()
    view.fit(boundingExtent, {padding: [10, 10, 10, 10]})

    const zoom = view.getZoom()

    if(zoom<20){
      view.setZoom(zoom-1)
    }else{
      view.setZoom(13)
    }

  }//end if..
}// end generateMap

/*line creating funtion*/
function createLine(pointsA, pointsB, map){
    var location1 = ol.proj.fromLonLat(pointsA);
    var location2 = ol.proj.fromLonLat(pointsB);

      var line2style = [
        new ol.style.Style({
          stroke: new ol.style.Stroke({
          color: '#d12710',
          width: 2
          })
        })
      ];

      var line2 = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [new ol.Feature({
            geometry: new ol.geom.LineString([location1, location2]),
            name: 'Line',
          })]
        })
      });

  line2.setStyle(line2style);
  map.addLayer(line2);
}
