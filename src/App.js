import './App.css';

import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import {useState, useEffect, createRef } from 'react'

import generateMap from './modules/openLayers.js'

function App() {

  const ace = window.ace
  const aceRef = createRef()

  const [data,setData] = useState({})

  const [title,setTitle] = useState("Tarebhir Ground, Kathmandu")

  const [images, setImages] = useState(["https://res.cloudinary.com/dpnxmo8ak/image/upload/c_scale,w_200/v1636788640/Travel%20App%20Photos/398a89ef8b53a648ddcfc81182ac08c0_mvkylq.jpg"])
  const [videos, setVideos] = useState(["https://res.cloudinary.com/dpnxmo8ak/image/upload/c_scale,w_200/v1636788640/Travel%20App%20Photos/398a89ef8b53a648ddcfc81182ac08c0_mvkylq.jpg"])

  const [locations, setLocations] = useState([
    {
        "name": "Jagdol",
        "points": [
            27.74239050078924,
            85.37913382899653
        ]
    },
    {
      "name": "Tarebhir Ground",
      "points": [
          27.770050442136302,
          85.3821017209774
      ]
    }

  ])

  useEffect(()=>{
    setData({title,images, videos, locations})
  },[title, images, locations, videos])

  useEffect(()=>{

    let editor = null
    ace.config.set('basePath', 'https://pagecdn.io/lib/ace/1.4.12/')

    editor = ace.edit(aceRef.current)
    editor.setTheme("ace/theme/github");
    editor.session.setMode("ace/mode/json");
    editor.setFontSize(16)
    editor.setValue(JSON.stringify(data, null, '\t'))  

    let datacopy = JSON.parse(JSON.stringify(data));  
    generateMap(document.getElementById('map'), datacopy )

  },[data, ace, aceRef])


  useEffect(()=>{


  },[])

  const doThis =(result)=>{
   const {destination, source } = result
   locations.splice(destination.index, 0, locations.splice(source.index,1)[0])
   setLocations([...locations])
  }

  const handleImages = (value, index)=>{
    images[index] = value
    setImages([...images])
  }

  const addImageField = ()=>{
    setImages([...images,""])
  }

  const handleVideos = (value, index)=>{
    videos[index] = value
    setVideos([...videos])
  }

  const addVideoField = ()=>{
    setVideos([...videos,""])
  }

  const removeVideo = (index)=>{
    videos.splice(index,1)
    setVideos([...videos])
  }

  const removeImageField = (index)=>{
    images.splice(index,1)
    setImages([...images])
  }

  const addLocationField = ()=>{
    setLocations([{name:"", points:[]},...locations])
  }

  const handleLocName = (value,index)=>{
    locations[index].name = value
    setLocations([...locations])
  }
  const handlePoint = (value,index)=>{

    let points = value.split(",").map((point)=>{
      return parseFloat(point)
    })
    locations[index].points = points
    setLocations([...locations])
  }

  const removeLocField = (index)=>{
    locations.splice(index,1)
    setLocations([...locations])
  }

  return (
    <div className="App">
      <div className="col">     
        <h1 className="font-bold text-3xl">Road Nepal - CMS - JSON Generator</h1>

        <div>
          <label className="font-bold">Title</label>
          <input type="text" 
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          onChange={(e)=>setTitle(e.target.value)} value={title}/>
        </div>

      <div>
        <label className="font-bold">Images</label> <br />
        {images.map((image, index)=>{
          return <div key={index}>
            <input type="text" 
            className="shadow w-5/6 appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            value={image}
            onChange={(e)=>handleImages(e.target.value,index)}/>
            <button 
            className="ml-2"
            onClick={()=>removeImageField(index)}>&#10060;</button>
          </div>
        })}
        <br />
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded"
          onClick={addImageField}>&#43; More Image</button>
      </div>
      <br />
      <hr />

      <div>
        <h3 className="font-bold">Videos</h3>

        {videos.map((video, index)=>{
          return <div key={index}>
            <input type="text" 
            className="shadow w-5/6 appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            value={video}
            onChange={(e)=>handleVideos(e.target.value,index)}/>
            <button 
            className="ml-2"
            onClick={()=>removeVideo(index)}>&#10060;</button>
          </div>
        })}
        <br />

        <button
          className="bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded"
          onClick={addVideoField}>&#43; More Video</button>
      </div>

      <hr />
      <div className="pt-5">
        <label className="font-bold">Locations / Maps</label><br/><br/>

          <DragDropContext onDragEnd={doThis}>

            <Droppable droppableId="locations">

              {(provided)=>(
               <div ref={provided.innerRef}  {...provided.droppableProps}>

               {locations.map((location, index)=>(
                <Draggable key={index} draggableId={"drag"+index} index={index}>
                  {(provided)=>(
                    <div className="mb-5 draggable__div"
                    {...provided.draggableProps} 
                    ref={provided.innerRef}
                    >
                    
                    <span className="bg-gray-500 text-white px-2 drag__handle" {...provided.dragHandleProps} >&#8597;</span>

                    <label className="px-2">Name</label><input 
                      className="w-1/2 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="text" onChange={(e)=>handleLocName(e.target.value,index)}
                      value={location.name}
                      />
                      <br />
                      <label className="px-2">Points</label><input 
                      className="w-1/2 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="text" onChange={(e)=>handlePoint(e.target.value,index)}
                      value={location.points}
                      />
                      <button className="ml-2" onClick={()=>removeLocField(index)}>&#10060;</button>
                      </div>
                  )}
                </Draggable>
                ))}
               {provided.placeholder}
               </div>
              )}
            </Droppable>
          </DragDropContext>

        
        <button className="bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded" onClick={addLocationField}>&#43; More Location</button>
      </div>

      <br />
      <hr />
      <br />
      <h2 className="font-bold">JSON</h2>

      <div id="ace" ref={aceRef} ></div>

      </div>

      <div id="map" className="col"></div>

    </div>
  );
}

export default App;